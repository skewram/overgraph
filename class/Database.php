<?php
class Database{
	function __construct($host, $port, $dbname, $user, $password){
		$this->host = $host;
		$this->port = $port;
		$this->dbname = $dbname;
		$this->user = $user;
		$this->password = $password;
	}
	function Connect(){
		try{
			$bdd = new PDO("mysql:host=".$this->host.";port=".$this->port.";charset=utf8;dbname=".$this->dbname, $this->user, $this->password);
			return $bdd;
		}catch(Exception $e){
			die($e->getMessage());
		}
	}
	function Disconnect(){
		$bdd = null;
		return $bdd;
	}
}