<?php
class User extends Database
{
	
	function __construct($username, $platforme, $region)
	{
		$Database = new Database("localhost", 3306, "overgraph", "root", "");
		$this->Database = $Database->Connect();
		$this->username = str_replace("#", "-", htmlspecialchars($username));
		$this->platforme = htmlspecialchars($platforme);
		$this->region = htmlspecialchars($region);
		$json_encode = file_get_contents("https://ow-api.com/v1/stats/".strtolower($this->platforme)."/".strtolower($this->region)."/".$this->username."/profile");
		$this->profile = json_decode($json_encode, true);
	}
	function CreateUser(){
		if(empty($this->profile["error"])){
			try{
				$request = $this->Database->prepare("SELECT * FROM users WHERE username = ? AND platforme = ? AND region = ?");
				$request->execute(array($this->username, $this->platforme, $this->region));
				$count = $request->rowCount();
				if($count == 0){
					try{
						$request = $this->Database->prepare("INSERT INTO users(username,platforme,region) VALUES (?,?,?)");
						$request->execute(array($this->username, $this->platforme, $this->region));
						print_r($request->errorInfo());
						return true;
					}catch(Exception $e){
						return false;
					}
				}
				return true;
			}catch(Exception $e){
				return false;
			}
		}else{
			return false;
		}
	}
	function Get_Username(){
		return $this->profile["name"];
	}
	function Get_Logo(){
		return $this->profile["icon"];
	}
	function Get_Level_Icon(){
		return $this->profile["levelIcon"];
	}
	function Get_Rating(){
		return $this->profile["rating"];
	}
	function Get_Rating_Icon(){
		return $this->profile["ratingIcon"];
	}
}