<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>OVERGRAPH | Stats Overwatch</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="jumbotron text-center">
	<div class="contents">
		<h1 class="display-1" id="main-text">Overgraph</h1>
		<hr style="width: 25%;border-color: white;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
				<section class="msg">
				<?php if(isset($_GET["error"])){
					switch($_GET["error"]){
						case 1:
							?>
							<div class="alert alert-danger">
								<p><b>Erreur:</b> Le nom d'utilisateur n'existe sur cette plateforme et/ou dans cette région !</p>
							</div>
							<?php
							break;
						case 2:
							?>
							<div class="alert alert-danger">
								<p><b>Erreur:</b> Veuillez compléter tous les champs !</p>
							</div>
							<?php
							break;
						default:
							break;
					}
				}
				?>
				</section>
					<form method="POST" action="stats.php" name="stats_form" id="stats_form">
						<div class="row">
							<input type="text" name="username" class="form-control form-control-lg form-inline col-lg-7" placeholder="Votre nom d'utilisateur">
							<select id="platforme" name="platforme" class="form-control form-control-lg form-inline col-lg-2">
								<option value="PC" selected>PC</option> 
								<option value="PS4">PS4</option>
								<option value="XBOX">XBOX</option>
							</select>
							<select id="region" name="region" class="form-control form-control-lg form-inline col-lg-3">
								<option value="EU" selected>Europe</option> 
								<option value="USA">États-Unis</option>
								<option value="AS">Asie</option>
							</select>
						</div>
						<input type="submit" value="Valider" class="btn btn-block btn-lg btn-primary" name="submit_stats_form">
					</form>
				</div>
				<div class="col-lg-4"></div>
			</div>
		</div>
	</div>
</div>
<section class="footer">
<p><b>Overgraph</b> made by <b>Skew</b> with ♥</p>
</section>
<!--JAVASCRIPT-->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>