<?php
include("class/Database.php");
include("class/Users.php");
/*[CHECK ERRORS]*/
if(isset($_POST["submit_stats_form"])){
    if(isset($_POST["username"]) && isset($_POST["platforme"]) && isset($_POST["region"]) && !empty($_POST["username"]) && !empty($_POST["platforme"]) && !empty($_POST["region"])){
        $User = new User($_POST["username"], $_POST["platforme"], $_POST["region"]);
        if($User->CreateUser() == false){
            header("Location:./index.php?error=1");
        }
    }else{
        header("Location:./index.php?error=2");
    }
}else{
    header("Location:./index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Stats Overwatch de <?=$User->Get_Username()?></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="jumbotron text-center">
	<div class="contents">
		<h1 class="display-1" id="main-text"><?=$User->Get_Username()?></h1>
		<hr style="width: 25%;border-color: white;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
                    <br>
                    <img id="icon" src="<?=$User->Get_Logo()?>">
                    <img id="level_icon" src="<?=$User->Get_Level_Icon()?>"></div>
                <div class="col-lg-4"></div>
            </div>
        </div>
    </div>
</div>
<section class="footer">
<p><b>Overgraph</b> made by <b>Skew</b> with ♥</p>
</section>
<!--JAVASCRIPT-->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>